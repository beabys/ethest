# ethest

## Challenge

* [Challenge](./Requirements.md)


## Project structure

```
├── Makefile
├── README.md
├── Requirements.md
├── go.sum
├── go.mod
├── cmd
│   └── server
│       └── main.go
└── internal
    ├── app
    │   ├── app.go
    │   ├── app_test.go
    │   ├── server
    │   │   ├── http.go
    │   │   ├── http_test.go
    │   │   ├── responses.go
    │   │   └── types.go
    │   └── types.go
    ├── clients
    │   ├── clients.go
    │   ├── clients_test.go
    │   └── types.go
    ├── ethereum
    │   ├── repository
    │   │   ├── repository.go
    │   │   ├── repository_test.go
    │   │   └── types.go
    │   ├── service.go
    │   ├── service_test.go
    │   └── types.go
    └── models
        └── transactions.go

```
## Commands

        make run           - run the app locally
        make test          - run unit test suites
        make unit-coverage - run unit test suites with coverage and export result into html page


## Run by local golang

If you do not install docker but you have installed golang and `GO111MODULE` enabled

You can run `make run` to run by local golang.

### Use different port

in case your port `8080` is in use, you can change adding the parameter `-server`
example

```
go run cmd/server/main.go -server=:3001
```
or in case binary has been compiled

```
{binary} -server=:3001
```
## Available endpoints

### Subscribe
```http
POST localhost:8080/subscribe  HTTP/1.1
Content-Type: application/json

{
    "address": "{address}"
}
```

### Get Current Block
```http
GET localhost:8080/current-block  HTTP/1.1

```

### Get Transactions
```http
GET localhost:8080/transactions/{address}  HTTP/1.1
```

