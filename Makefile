SHELL:=/bin/bash

.PHONY: run
run:
	go mod tidy && go run cmd/server/main.go

.PHONY: unit
unit:
	go mod tidy && go test ./internal/... -race -coverprofile .testCoverage.txt

.PHONY: unit-coverage
unit-coverage: unit ## Runs unit tests and generates a html coverage report
	go tool cover -html=.testCoverage.txt -o unit.html

