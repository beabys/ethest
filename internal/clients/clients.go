package clients

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
)

func New(host string, client HTTPClient) JsonRpcClientInterface {
	return &JsonRpcClient{
		host:   host,
		client: client,
	}
}

func (c *JsonRpcClient) Call(args any) ([]byte, error) {
	bodyBytes, err := json.Marshal(args)
	if err != nil {
		return nil, fmt.Errorf("not able to marshal body request: %v", err)
	}

	body := strings.NewReader(string(bodyBytes))
	req, err := http.NewRequest(http.MethodPost, c.host, body)
	if err != nil {
		return nil, fmt.Errorf("error preparing new request: %v", err)
	}
	req.Header.Add("Content-Type", "application/json")
	response, err := c.client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error sending request: %v", err)
	}
	defer response.Body.Close()
	bodyBytes, err = io.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading body from response: %w", err)
	}
	return bodyBytes, nil
}
