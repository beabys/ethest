package clients

import (
	"net/http"
)

type HTTPClient interface {
	Do(*http.Request) (*http.Response, error)
}

type JsonRpcClientInterface interface {
	Call(args any) ([]byte, error)
}

type JsonRpcClient struct {
	host   string
	client HTTPClient
}
