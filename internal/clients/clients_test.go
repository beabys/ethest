package clients

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestJsonRpcClient_Call(t *testing.T) {
	tests := []struct {
		name          string
		want          []byte
		host          string
		client        HTTPClient
		args          any
		wantErr       bool
		errorContains string
	}{
		{
			name: "error_unmarshal_args",
			host: ":3000",
			args: map[string]interface{}{
				"foo": make(chan int),
			},
			errorContains: "not able to marshal body request",
			wantErr:       true,
		},
		{
			name: "error_preparing_newRequest",
			host: ":3000shouldfails",
			args: map[string]any{
				"jsonrpc": "2.0",
				"method":  "example",
				"id":      1,
			},
			errorContains: "error preparing new request",
			wantErr:       true,
		},
		{
			name: "error_from_client",
			host: "http://client.com",
			args: map[string]any{
				"jsonrpc": "2.0",
				"method":  "example",
				"id":      1,
			},
			client: &mockHttpClient{
				DoFunc: func(*http.Request) (*http.Response, error) {
					return nil, fmt.Errorf("error from client")
				},
			},
			errorContains: "error sending request",
			wantErr:       true,
		},
		{
			name: "error_from_client",
			host: "http://client.com",
			args: map[string]any{
				"jsonrpc": "2.0",
				"method":  "example",
				"id":      1,
			},
			client: &mockHttpClient{
				DoFunc: func(*http.Request) (*http.Response, error) {
					r := &errorReader{}
					resp := &http.Response{
						StatusCode: 200,
						Body:       r,
					}
					resp.Body.Close()
					return resp, nil
				},
			},
			errorContains: "error reading body from response",
			wantErr:       true,
		},
		{
			name: "success_response",
			host: "http://client.com",
			args: map[string]any{
				"jsonrpc": "2.0",
				"method":  "example",
				"id":      1,
			},
			want: []byte(`{"name":"Test"}`),
			client: &mockHttpClient{
				DoFunc: func(*http.Request) (*http.Response, error) {
					r := io.NopCloser(bytes.NewReader([]byte(`{"name":"Test"}`)))
					resp := &http.Response{
						StatusCode: 200,
						Body:       r,
					}
					resp.Body.Close()
					return resp, nil
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			client := New(tt.host, tt.client)
			result, err := client.Call(tt.args)
			if tt.wantErr {
				assert.ErrorContains(t, err, tt.errorContains)
			}
			assert.Equal(t, tt.want, result)
		})
	}
}

type mockHttpClient struct {
	DoFunc func(*http.Request) (*http.Response, error)
}

func (m *mockHttpClient) Do(r *http.Request) (*http.Response, error) {
	return m.DoFunc(r)
}

type errorReader struct{}

func (er *errorReader) Read(p []byte) (n int, err error) {
	return 0, fmt.Errorf("failed reading")
}

func (br *errorReader) Close() error {
	return fmt.Errorf("failed closing")
}
