package app

import (
	"gitlab.com/beabys/ethest/internal/app/server"
	"gitlab.com/beabys/ethest/internal/clients"
	"gitlab.com/beabys/ethest/internal/ethereum"
)

// App is the Application Struct
type App struct {
	HttpServer server.ApiServer
	Clients    Clients
	EthService ethereum.Parser
}

type Clients struct {
	JsonRpcClient clients.JsonRpcClientInterface
}
