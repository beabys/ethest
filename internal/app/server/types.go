package server

import (
	"context"
	"net/http"

	"gitlab.com/beabys/ethest/internal/ethereum"
	"golang.org/x/sync/errgroup"
)

// HttpServer is a struct of an Http Server
type HttpServer struct {
	MuxRouter       *http.ServeMux
	Server          *http.Server
	EthereumService ethereum.Parser
}

type subscribeRequest struct {
	Address string `json:"address"`
}

type Response struct {
	Result any `json:"result"`
}

type ApiServer interface {
	Run(context.Context, *errgroup.Group)
}
