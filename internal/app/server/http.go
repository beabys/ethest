package server

import (
	"context"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"regexp"
	"strings"
	"time"

	"golang.org/x/sync/errgroup"
)

const (
	applicationJson string = "application/json"
)

// NewHttpServer returns a new pointer of HttpServer
func NewHttpServer() *HttpServer {
	return &HttpServer{}
}

// Run implements Run api server function for Http server
func (hs *HttpServer) Run(ctx context.Context, wg *errgroup.Group) {
	wg.Go(func() error {
		log.Print("http server started on ", hs.Server.Addr)
		if err := hs.Server.ListenAndServe(); err != nil {
			if errors.Is(err, http.ErrServerClosed) {
				return nil
			}
			log.Print("http server stopped with error", err)
			return err
		}
		return nil
	})

	wg.Go(func() error {
		<-ctx.Done()
		log.Print("shutting down gracefully http server")
		ctxTimeout, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := hs.Server.Shutdown(ctxTimeout); err != nil {
			log.Print("error shutting server down", err)
			return err
		}
		return nil
	})
}

func (hs *HttpServer) SetMuxRouter() {
	router := http.NewServeMux()

	router.HandleFunc("POST /subscribe", hs.subscribe)

	router.HandleFunc("GET /current-block", hs.getCurrentBlock)

	router.HandleFunc("GET /transactions/{address}", hs.getTransactions)

	hs.MuxRouter = router

}

func (hs *HttpServer) subscribe(w http.ResponseWriter, r *http.Request) {
	subscribeRequest := &subscribeRequest{}
	// validate content type
	if !validJsonMediaType(r) {
		responseWriter(w, http.StatusUnsupportedMediaType, "invalid content-type")
		return
	}
	// read request body
	if err := json.NewDecoder(r.Body).Decode(subscribeRequest); err != nil {
		log.Printf("Error reading body: %v", err)
		responseWriter(w, http.StatusBadRequest, "can't read body")
		return
	}

	if !isValidAddress(subscribeRequest.Address) {
		responseWriter(w, http.StatusBadRequest, "invalid address")
		return
	}
	// call service
	result := hs.EthereumService.Subscribe(subscribeRequest.Address)
	statusCode := http.StatusOK
	if !result {
		statusCode = http.StatusBadRequest
	}
	responseWriter(w, statusCode, result)
}

func (hs *HttpServer) getCurrentBlock(w http.ResponseWriter, r *http.Request) {
	result := hs.EthereumService.GetCurrentBlock()
	responseWriter(w, http.StatusOK, result)

}

func (hs *HttpServer) getTransactions(w http.ResponseWriter, r *http.Request) {
	address := r.PathValue("address")
	if !isValidAddress(address) {
		responseWriter(w, http.StatusBadRequest, "invalid address")
		return
	}
	result := hs.EthereumService.GetTransactions(address)
	code := http.StatusOK
	if result == nil {
		code = http.StatusUnauthorized
	}
	responseWriter(w, code, result)
}

func validJsonMediaType(r *http.Request) bool {
	ctHeader := r.Header.Get("Content-Type")
	if ctHeader == "" {
		return false
	}
	contentType := strings.ToLower(strings.TrimSpace(strings.Split(ctHeader, ";")[0]))
	return contentType == applicationJson
}

func isValidAddress(s string) bool {
	rexp := regexp.MustCompile("^0x[0-9a-fA-F]{40}$")
	return rexp.MatchString(s)
}
