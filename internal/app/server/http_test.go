package server

import (
	"bytes"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/beabys/ethest/internal/ethereum"
	"gitlab.com/beabys/ethest/internal/models"
)

func TestHttpServer_SetMuxRouter(t *testing.T) {
	tests := []struct {
		name           string
		method         string
		path           string
		handler        func(ethereum.Parser) func(http.ResponseWriter, *http.Request)
		expectedStatus int
		expected       string
		mockService    ethereum.Parser
		body           io.Reader
		extraHeaders   map[string]string
		pathValue      map[string]string
	}{
		{
			name:           "test_post_subscribe_fail_no_content_type",
			method:         "POST",
			path:           "/subscribe",
			expectedStatus: http.StatusUnsupportedMediaType,
			expected:       "{\"result\":\"invalid content-type\"}\n",
			handler: func(eth ethereum.Parser) func(http.ResponseWriter, *http.Request) {
				httpServer := NewHttpServer()
				httpServer.EthereumService = eth
				return httpServer.subscribe
			},
			mockService: &mockEthereumService{},
			body:        nil,
		},
		{
			name:           "test_post_subscribe_fail_encoding",
			method:         "POST",
			path:           "/subscribe",
			expectedStatus: http.StatusUnsupportedMediaType,
			expected:       "{\"result\":\"invalid content-type\"}\n",
			handler: func(eth ethereum.Parser) func(http.ResponseWriter, *http.Request) {
				httpServer := NewHttpServer()
				httpServer.EthereumService = eth
				return httpServer.subscribe
			},
			mockService: &mockEthereumService{},
			body:        nil,
			extraHeaders: map[string]string{
				"Content-Type": "text/plain",
			},
		},
		{
			name:           "test_error_subscribe_requets",
			method:         "POST",
			path:           "/subscribe",
			expectedStatus: http.StatusBadRequest,
			expected:       "{\"result\":\"can't read body\"}\n",
			handler: func(eth ethereum.Parser) func(http.ResponseWriter, *http.Request) {
				httpServer := NewHttpServer()
				httpServer.EthereumService = eth
				return httpServer.subscribe
			},
			mockService: &mockEthereumService{},
			body:        bytes.NewReader([]byte("")),
			extraHeaders: map[string]string{
				"Content-Type": "application/json",
			},
		},
		{
			name:           "test_subscribe_invalid_address",
			method:         "POST",
			path:           "/subscribe",
			expectedStatus: http.StatusBadRequest,
			expected:       "{\"result\":\"invalid address\"}\n",
			handler: func(eth ethereum.Parser) func(http.ResponseWriter, *http.Request) {
				httpServer := NewHttpServer()
				httpServer.EthereumService = eth
				return httpServer.subscribe
			},
			body: bytes.NewReader([]byte(`{"address": "0x000000000022lmndee9f6b43ac78ba3"}`)),
			extraHeaders: map[string]string{
				"Content-Type": "application/json",
			},
		},
		{
			name:           "test_address_registered",
			method:         "POST",
			path:           "/subscribe",
			expectedStatus: http.StatusBadRequest,
			expected:       "{\"result\":false}\n",
			handler: func(eth ethereum.Parser) func(http.ResponseWriter, *http.Request) {
				httpServer := NewHttpServer()
				httpServer.EthereumService = eth
				return httpServer.subscribe
			},
			mockService: &mockEthereumService{
				subscribeFunc: func(_ string) bool {
					return false
				},
			},
			body: bytes.NewReader([]byte(`{"address": "0x000000000022d473030f116ddee9f6b43ac78ba3"}`)),
			extraHeaders: map[string]string{
				"Content-Type": "application/json",
			},
		},
		{
			name:           "test_subscribe_success",
			method:         "POST",
			path:           "/subscribe",
			expectedStatus: http.StatusOK,
			expected:       "{\"result\":true}\n",
			handler: func(eth ethereum.Parser) func(http.ResponseWriter, *http.Request) {
				httpServer := NewHttpServer()
				httpServer.EthereumService = eth
				return httpServer.subscribe
			},
			mockService: &mockEthereumService{
				subscribeFunc: func(_ string) bool {
					return true
				},
			},
			body: bytes.NewReader([]byte(`{"address": "0x000000000022d473030f116ddee9f6b43ac78ba3"}`)),
			extraHeaders: map[string]string{
				"Content-Type": "application/json",
			},
		},
		{
			name:           "test_GetCurrentBlock_success",
			method:         "GET",
			path:           "/current-block",
			expectedStatus: http.StatusOK,
			expected:       "{\"result\":10}\n",
			handler: func(eth ethereum.Parser) func(http.ResponseWriter, *http.Request) {
				httpServer := NewHttpServer()
				httpServer.EthereumService = eth
				return httpServer.getCurrentBlock
			},
			mockService: &mockEthereumService{
				getCurrentBlockFunc: func() int {
					return 10
				},
			},
			body: nil,
			extraHeaders: map[string]string{
				"Content-Type": "application/json",
			},
		},
		{
			name:           "test_Gettransactions_invalid_address",
			method:         "GET",
			path:           "/transactions/0x000000000022lmndee9f6b43ac78ba3",
			expectedStatus: http.StatusBadRequest,
			expected:       "{\"result\":\"invalid address\"}\n",
			handler: func(eth ethereum.Parser) func(http.ResponseWriter, *http.Request) {
				httpServer := NewHttpServer()
				httpServer.EthereumService = eth
				return httpServer.getTransactions
			},
			mockService: &mockEthereumService{
				getTransactionsFunc: func(_ string) []models.Transaction {
					return nil
				},
			},
			body: nil,
			extraHeaders: map[string]string{
				"Content-Type": "application/json",
			},
			pathValue: map[string]string{
				"address": "0x000000000022lmndee9f6b43ac78ba3",
			},
		},
		{
			name:           "test_Gettransactions_empty",
			method:         "GET",
			path:           "/transactions/0x000000000022d473030f116ddee9f6b43ac78ba3",
			expectedStatus: http.StatusUnauthorized,
			expected:       "{\"result\":null}\n",
			handler: func(eth ethereum.Parser) func(http.ResponseWriter, *http.Request) {
				httpServer := NewHttpServer()
				httpServer.EthereumService = eth
				return httpServer.getTransactions
			},
			mockService: &mockEthereumService{
				getTransactionsFunc: func(_ string) []models.Transaction {
					return nil
				},
			},
			body: nil,
			extraHeaders: map[string]string{
				"Content-Type": "application/json",
			},
			pathValue: map[string]string{
				"address": "0x000000000022d473030f116ddee9f6b43ac78ba3",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest(tt.method, tt.path, tt.body)
			if err != nil {
				t.Fatal(err)
			}
			for k, v := range tt.pathValue {
				req.SetPathValue(k, v)
			}
			for kh, vh := range tt.extraHeaders {
				req.Header.Add(kh, vh)
			}
			rec := httptest.NewRecorder()
			handler := http.HandlerFunc(tt.handler(tt.mockService))
			handler.ServeHTTP(rec, req)

			assert.Equal(t, tt.expectedStatus, rec.Code)
			assert.Equal(t, tt.expected, rec.Body.String())
		})
	}
}

type mockEthereumService struct {
	getCurrentBlockFunc func() int
	subscribeFunc       func(address string) bool
	getTransactionsFunc func(address string) []models.Transaction
}

func (m *mockEthereumService) GetCurrentBlock() int {
	return m.getCurrentBlockFunc()
}

func (m *mockEthereumService) Subscribe(address string) bool {
	return m.subscribeFunc(address)
}

func (m *mockEthereumService) GetTransactions(address string) []models.Transaction {
	return m.getTransactionsFunc(address)
}
