package app

import (
	"net/http"
	"time"

	"gitlab.com/beabys/ethest/internal/app/server"
	"gitlab.com/beabys/ethest/internal/clients"
	"gitlab.com/beabys/ethest/internal/ethereum"
	"gitlab.com/beabys/ethest/internal/ethereum/repository"
)

func New() *App {
	return &App{}
}

func (a *App) SetUp(ce string) *App {
	//set clients
	client := clients.New(ce, &http.Client{})
	a.Clients.JsonRpcClient = client

	//set services
	ethereumRepository := repository.New()
	ethereumService := ethereum.New(client, ethereumRepository)
	a.EthService = ethereumService
	return a
}

func (a *App) SetHTTPServer(address string) error {
	httpServer := server.NewHttpServer()
	httpServer.SetMuxRouter()
	httpServer.Server = &http.Server{
		Addr:              address,
		Handler:           httpServer.MuxRouter,
		ReadHeaderTimeout: time.Duration(30 * 1000),
	}
	httpServer.EthereumService = a.EthService

	a.HttpServer = httpServer

	return nil
}
