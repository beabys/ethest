package app

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/beabys/ethest/internal/app/server"
	"gitlab.com/beabys/ethest/internal/clients"
	"gitlab.com/beabys/ethest/internal/ethereum"
)

func TestApp_SetUp(t *testing.T) {
	t.Run("test_setup", func(t *testing.T) {
		app := New()
		app.SetUp("http://example.com")
		client := &clients.JsonRpcClient{}
		service := &ethereum.EthereumService{}
		assert.IsType(t, app.Clients.JsonRpcClient, client)
		assert.IsType(t, app.EthService, service)
	})

	t.Run("test_setupH_ttpServer", func(t *testing.T) {
		app := New()
		app.SetHTTPServer(":3000")
		server := &server.HttpServer{}
		assert.IsType(t, app.HttpServer, server)
	})
}
