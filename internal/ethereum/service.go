package ethereum

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"

	"gitlab.com/beabys/ethest/internal/clients"
	"gitlab.com/beabys/ethest/internal/ethereum/repository"
	"gitlab.com/beabys/ethest/internal/models"
)

const (
	JsonRPCVersion string = "2.0"
	BlockNumber    string = "eth_blockNumber"
	GetLogs        string = "eth_getLogs"
)

func New(client clients.JsonRpcClientInterface, repository repository.EthereumRepositoryInterface) Parser {
	return &EthereumService{client, repository}
}

// GetCurrentBlock is the service implementation to get last parsed block
func (e *EthereumService) GetCurrentBlock() int {
	args := &Request{
		JsonRPC: JsonRPCVersion,
		Method:  BlockNumber,
		Params:  []any{},
		ID:      1,
	}
	response := &Response{}
	if err := e.makeRequest(args, response); err != nil {
		log.Println(err)
		return 0
	}
	res, err := strconv.ParseInt(strings.TrimPrefix(response.Result, "0x"), 16, 64)
	if err != nil {
		log.Println(err)
		return 0
	}
	return int(res)
}

// Subscribe is the service implementation to subscribe one address
func (e *EthereumService) Subscribe(address string) bool {
	if err := e.repository.SaveAddress(address); err != nil {
		log.Print("error saving address or already exist", address, err.Error())
		return false
	}
	return true
}

// GetTransactions is the service implementation to list of inbound or outbound transactions for one address
func (e *EthereumService) GetTransactions(address string) []models.Transaction {
	if !e.repository.AddressExist(address) {
		log.Println("address not subscribed", address)
		return nil
	}
	mapArgs := map[string]string{
		"address": address,
	}
	args := &Request{
		JsonRPC: JsonRPCVersion,
		Method:  GetLogs,
		Params:  []any{mapArgs},
		ID:      1,
	}
	response := &ResponseTransaction{}
	if err := e.makeRequest(args, response); err != nil {
		log.Println(err)
		return nil
	}
	return response.Result
}

// makeRequest is a private function to make request to the client
func (e *EthereumService) makeRequest(args *Request, response any) error {
	resp, err := e.client.Call(args)
	if err != nil {
		return fmt.Errorf("error from request: %v", err)
	}
	// try to unmarshall the response
	if err := json.Unmarshal(resp, &response); err != nil {
		return fmt.Errorf("error parsing body from response: %v", err)
	}
	return nil
}
