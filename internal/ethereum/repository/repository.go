package repository

import "fmt"

func New() *EthereumMemoryRepository {
	return &EthereumMemoryRepository{}
}

func (e *EthereumMemoryRepository) SaveAddress(a string) error {
	if e.AddressExist(a) {
		return fmt.Errorf("address already exist")
	}
	e.Addresses = append(e.Addresses, a)
	return nil
}

func (e *EthereumMemoryRepository) AddressExist(a string) bool {
	for _, val := range e.Addresses {
		if val == a {
			return true
		}
	}
	return false
}
