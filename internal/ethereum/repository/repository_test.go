package repository

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEthereumMemoryRepository(t *testing.T) {
	tests := []struct {
		name            string
		address         string
		addressExisting []string
		wantErr         bool
	}{
		{
			name:            "error_saving_address",
			address:         "0x000000000022d473030f116ddee9f6b43ac78ba3",
			addressExisting: []string{"0x000000000022d473030f116ddee9f6b43ac78ba3"},
			wantErr:         true,
		},
		{
			name:    "no_error",
			address: "0x000000000022d473030f116ddee9f6b43ac78ba3",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repository := New()
			if len(tt.addressExisting) > 0 {
				repository.Addresses = append(repository.Addresses, tt.addressExisting...)
			}
			err := repository.SaveAddress(tt.address)
			if tt.wantErr {
				assert.ErrorContains(t, err, "address already exist")
				return
			}
			assert.NoError(t, err)
		})
	}
}
