package repository

type EthereumRepositoryInterface interface {
	SaveAddress(string) error
	AddressExist(string) bool
}

type EthereumMemoryRepository struct {
	Addresses []string
}
