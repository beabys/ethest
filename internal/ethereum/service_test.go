package ethereum

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/beabys/ethest/internal/clients"
	"gitlab.com/beabys/ethest/internal/ethereum/repository"
	"gitlab.com/beabys/ethest/internal/models"
)

func TestEthereumService_GetCurrentBlock(t *testing.T) {
	tests := []struct {
		name       string
		client     clients.JsonRpcClientInterface
		repository repository.EthereumRepositoryInterface
		want       int
	}{
		{
			name: "error_from_client",
			client: &mockClient{
				func(args any) ([]byte, error) {
					return nil, fmt.Errorf("error in client")
				},
			},
			want: 0,
		},
		{
			name: "error_unmarshall_response",
			client: &mockClient{
				func(args any) ([]byte, error) {
					return nil, nil
				},
			},
			want: 0,
		},
		{
			name: "error_parsing_hex_response",
			client: &mockClient{
				func(args any) ([]byte, error) {
					result := `{"jsonrpc": "2.0", "id": 1, "result":"not an hex"}`
					return []byte(result), nil
				},
			},
			want: 0,
		},
		{
			name: "success_response",
			client: &mockClient{
				func(args any) ([]byte, error) {
					result := `{"jsonrpc": "2.0", "id": 1, "result":"0x000A"}`
					return []byte(result), nil
				},
			},
			want: 10,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			service := New(tt.client, tt.repository)
			result := service.GetCurrentBlock()
			assert.Equal(t, result, tt.want)
		})
	}
}

func TestEthereumService_Subscribe(t *testing.T) {
	tests := []struct {
		name       string
		client     clients.JsonRpcClientInterface
		repository repository.EthereumRepositoryInterface
		address    string
		want       bool
	}{
		{
			name: "error_from_repository",
			repository: &mockRepository{
				SaveAddressFunc: func(string) error {
					return fmt.Errorf("error from repository")
				},
			},
			address: "0x000000000022d473030f116ddee9f6b43ac78ba3",
			want:    false,
		},
		{
			name: "success_response",
			repository: &mockRepository{
				SaveAddressFunc: func(string) error {
					return nil
				},
			},
			address: "0x000000000022d473030f116ddee9f6b43ac78ba3",
			want:    true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			service := New(tt.client, tt.repository)
			result := service.Subscribe(tt.address)
			assert.Equal(t, result, tt.want)
		})
	}
}

func TestEthereumService_GetTransactions(t *testing.T) {
	tests := []struct {
		name       string
		client     clients.JsonRpcClientInterface
		repository repository.EthereumRepositoryInterface
		address    string
		want       []models.Transaction
	}{
		{
			name: "error_from_repository_address_not_subscribed",
			repository: &mockRepository{
				IsAddressExist: false,
			},
			address: "0x000000000022d473030f116ddee9f6b43ac78ba3",
			want:    nil,
		},
		{
			name: "error_in_client",
			repository: &mockRepository{
				IsAddressExist: true,
			},
			client: &mockClient{
				func(args any) ([]byte, error) {
					return nil, fmt.Errorf("error in client")
				},
			},
			address: "0x000000000022d473030f116ddee9f6b43ac78ba3",
			want:    nil,
		},
		{
			name: "success_response",
			repository: &mockRepository{
				IsAddressExist: true,
			},

			client: &mockClient{
				func(args any) ([]byte, error) {
					return []byte(transactions()), nil
				},
			},
			address: "0x000000000022d473030f116ddee9f6b43ac78ba3",
			want:    resultTransactions(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			service := New(tt.client, tt.repository)
			result := service.GetTransactions(tt.address)
			assert.Equal(t, result, tt.want)
		})
	}
}

type mockClient struct {
	CallFunc func(args any) ([]byte, error)
}

func (m *mockClient) Call(args any) ([]byte, error) {
	return m.CallFunc(args)
}

type mockRepository struct {
	SaveAddressFunc func(string) error
	IsAddressExist  bool
}

func (r *mockRepository) SaveAddress(a string) error {
	return r.SaveAddressFunc(a)
}

func (r *mockRepository) AddressExist(_ string) bool {
	return r.IsAddressExist
}

func transactions() string {
	return `{
		"jsonrpc": "2.0",
		"result": [
			{
				"address": "0x000000000022d473030f116ddee9f6b43ac78ba3",
				"topics": [
					"0xc6a377bfc4eb120024a8ac08eef205be16b817020812c73223e81d1bdb9708ec",
					"0x0000000000000000000000006df46f259665bcc6fca351b3915462fe9d0b4d8a",
					"0x000000000000000000000000eb4f5c4bf62fac1be7e4bef9c7c055bf1c52241d",
					"0x0000000000000000000000003fc91a3afd70395cd496c647d5a6cc9d4b2b7fad"
				],
				"data": "0x000000000000000000000000ffffffffffffffffffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000664b46fb0000000000000000000000000000000000000000000000000000000000000000",
				"blockNumber": "0x12c8c60",
				"transactionHash": "0xe1f832fd74d36f7551b1cf68643c7cf3786f8380e73774f8290ed58d1d40f05d",
				"transactionIndex": "0x12",
				"blockHash": "0x2189db889dd12db6e37f6464d621bcbcd92c9edd9f54b4c78297ffab0ce8a02d",
				"logIndex": "0x29",
				"removed": false
			},
			{
				"address": "0x000000000022d473030f116ddee9f6b43ac78ba3",
				"topics": [
					"0xc6a377bfc4eb120024a8ac08eef205be16b817020812c73223e81d1bdb9708ec",
					"0x000000000000000000000000ecdbe191ee42dc25360a2b6d2b4c771c4bebba8c",
					"0x000000000000000000000000ae697f994fc5ebc000f8e22ebffee04612f98a0d",
					"0x0000000000000000000000003fc91a3afd70395cd496c647d5a6cc9d4b2b7fad"
				],
				"data": "0x000000000000000000000000ffffffffffffffffffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000664b47160000000000000000000000000000000000000000000000000000000000000000",
				"blockNumber": "0x12c8c60",
				"transactionHash": "0xf27ce032c399d8bacce2973275a5aa382c3827bde33fb910aefc4fcf9dd98730",
				"transactionIndex": "0x24",
				"blockHash": "0x2189db889dd12db6e37f6464d621bcbcd92c9edd9f54b4c78297ffab0ce8a02d",
				"logIndex": "0x65",
				"removed": false
			}
		]
	}`
}

func resultTransactions() []models.Transaction {
	return []models.Transaction{
		{
			Address: "0x000000000022d473030f116ddee9f6b43ac78ba3",
			Topics: []string{
				"0xc6a377bfc4eb120024a8ac08eef205be16b817020812c73223e81d1bdb9708ec",
				"0x0000000000000000000000006df46f259665bcc6fca351b3915462fe9d0b4d8a",
				"0x000000000000000000000000eb4f5c4bf62fac1be7e4bef9c7c055bf1c52241d",
				"0x0000000000000000000000003fc91a3afd70395cd496c647d5a6cc9d4b2b7fad",
			},
			Data:             "0x000000000000000000000000ffffffffffffffffffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000664b46fb0000000000000000000000000000000000000000000000000000000000000000",
			BlockNumber:      "0x12c8c60",
			TransactionHash:  "0xe1f832fd74d36f7551b1cf68643c7cf3786f8380e73774f8290ed58d1d40f05d",
			TransactionIndex: "0x12",
			BlockHash:        "0x2189db889dd12db6e37f6464d621bcbcd92c9edd9f54b4c78297ffab0ce8a02d",
			LogIndex:         "0x29",
			Removed:          false,
		},
		{
			Address: "0x000000000022d473030f116ddee9f6b43ac78ba3",
			Topics: []string{
				"0xc6a377bfc4eb120024a8ac08eef205be16b817020812c73223e81d1bdb9708ec",
				"0x000000000000000000000000ecdbe191ee42dc25360a2b6d2b4c771c4bebba8c",
				"0x000000000000000000000000ae697f994fc5ebc000f8e22ebffee04612f98a0d",
				"0x0000000000000000000000003fc91a3afd70395cd496c647d5a6cc9d4b2b7fad",
			},
			Data:             "0x000000000000000000000000ffffffffffffffffffffffffffffffffffffffff00000000000000000000000000000000000000000000000000000000664b47160000000000000000000000000000000000000000000000000000000000000000",
			BlockNumber:      "0x12c8c60",
			TransactionHash:  "0xf27ce032c399d8bacce2973275a5aa382c3827bde33fb910aefc4fcf9dd98730",
			TransactionIndex: "0x24",
			BlockHash:        "0x2189db889dd12db6e37f6464d621bcbcd92c9edd9f54b4c78297ffab0ce8a02d",
			LogIndex:         "0x65",
			Removed:          false,
		},
	}
}
