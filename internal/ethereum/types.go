package ethereum

import (
	"gitlab.com/beabys/ethest/internal/clients"
	"gitlab.com/beabys/ethest/internal/ethereum/repository"
	"gitlab.com/beabys/ethest/internal/models"
)

type Parser interface {
	//last parsed block
	GetCurrentBlock() int

	// add address to observer
	Subscribe(address string) bool

	// list of inbound or outbound transactions for one address
	GetTransactions(address string) []models.Transaction
}

type EthereumService struct {
	client     clients.JsonRpcClientInterface
	repository repository.EthereumRepositoryInterface
}

type Request struct {
	JsonRPC string `json:"jsonrpc"`
	Method  string `json:"method"`
	Params  []any  `json:"params"`
	ID      int    `json:"id"`
}

type Response struct {
	Jsonrpc string `json:"jsonrpc"`
	ID      int    `json:"id"`
	Result  string `json:"result"`
}
type ResponseTransaction struct {
	Jsonrpc string               `json:"jsonrpc"`
	Result  []models.Transaction `json:"result"`
}
