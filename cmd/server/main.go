package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/beabys/ethest/internal/app"
	"golang.org/x/sync/errgroup"
)

const (
	DEFAULT_CLIENT_ENDPOINT    = "https://cloudflare-eth.com"
	DEFAULT_HTTPSERVER_ADDRESS = ":8080"
)

func main() {
	// First we Set a context and a stopFn
	ctx, stopFn := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill, syscall.SIGTERM)
	defer stopFn()

	// new app
	app := app.New()

	var client_endpoint, httpServerAddress string
	flag.StringVar(&client_endpoint, "client", DEFAULT_CLIENT_ENDPOINT, "client endpoint")
	flag.StringVar(&httpServerAddress, "server", DEFAULT_HTTPSERVER_ADDRESS, "address:port used for http server")

	flag.Parse()

	// Prepare clients and services
	app.SetUp(client_endpoint)

	// set httpServer
	app.SetHTTPServer(httpServerAddress)

	wg, ctx := errgroup.WithContext(ctx)

	// run server
	app.HttpServer.Run(ctx, wg)

	err := wg.Wait()
	if err != nil {
		log.Print("application stopped with error:", err)
	}
	log.Print("application stopped")

}
